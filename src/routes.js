"use strict";
var React =require('react');

var Router=require('react-router');
var DefaultRoute=Router.DefaultRoute;
var Route=Router.Route;
var NotFoundRoute =Router.NotFoundRoute;
var Redirect=Router.Redirect;

var routes=(
<Route name="app" path="/" handler={require('./components/app')}>
<DefaultRoute handler={require('./components/homePage')}/>
<Route name="employees" path="/employees" handler={require('./components/employees/employeePage')}/>
<Route name="addEmployee" path="/empl" handler={require('./components/employees/manageEmployees')}/>
<Route name="editEmployee" path="/empl/:id" handler={require('./components/employees/manageEmployees')}/>
<Route name="about" path="/about" handler={require('./components/about/aboutPage')}/>
<NotFoundRoute handler={require('./components/notFound')}/> 
<Redirect from="about-us" to="about"/>
<Redirect from="employee" to="employees"/>
<Redirect from="about/*" to="about"/>
</Route>

);

module.exports=routes;