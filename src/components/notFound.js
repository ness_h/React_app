"use strict";

var React = require('react');
var Router =require('react-router');
var Link=Router.Link;

var NotFound = React.createClass ({
render: function () {
    return ( 
  <div className="container">
  <div className="page-header">
    <h1>Sorry!!! not found</h1>      
  </div>
  <Link to="app" className="btn btn-primary btn-lg">App</Link>
  </div>
    )
}
});

module.exports=NotFound;