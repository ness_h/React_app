"use strict";

var React = require('react');

var About = React.createClass({
    statics: {
        willTransitionTo: function (transition, params, query, callback) {
            if (!confirm('are u sure?')) {
                transition.about();
            } else {
                callback();
            }
        },
        willTransitionFrom: function (transition, component) {
            if (!confirm('Are you sure?')) {
                transition.about();
            }
        }
    },


    render: function () {
        return ( <div>
            <div className = "page-header" >
            <h1> Menu </h1>       
             </div> <h2> Food </h2> <ul>
            <li> Coffee </li> 
            <li> Tea </li> 
            <li> Milk </li> </ul> </div>
        )
    }
});

module.exports = About;
