"use strict";

var React=require('react');
var EmployeeApi =require('../../api/employeeApi');
var Input = require('../common/textInput');

var EmployeesForm = React.createClass({

    propTypes: {
        employee: React.PropTypes.object.isRequired,
        onSave: React.PropTypes.func.isRequired,
        onChange: React.PropTypes.func.isRequired,
        errors: React.PropTypes.object
    },
    
        render:function(){
        return (
        <div>
        <form>
                <Input
                    name="firstName"
                    label="First Name"
                    value={this.props.employee.firstName}
                    onChange={this.props.onChange}
                    error={this.props.errors.firstName} />

                <Input
                    name="lastName"
                    label="Last Name"
                    value={this.props.employee.lastName}
                    onChange={this.props.onChange}
                    error={this.props.errors.lastName} />
                 <br></br>
                <input type="submit" className="btn btn-default" value="Submit" onClick={this.props.onSave}/>
        </form>
        </div>
        );
    }
});

module.exports=EmployeesForm;