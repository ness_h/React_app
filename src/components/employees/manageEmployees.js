"use strict";

var React=require('react');
var Router=require('react-router');

//var EmployeeApi =require('../../api/employeeApi');
var EmployeeActions= require('../../actions/employeeActions');
var EmployeeStore= require('../../stores/employeeStore');
var EmployeeForm =require('./employeesForm');
var toastr =require('toastr');

var ManageEmployees = React.createClass({
	mixins:[
	Router.Navigation
	],

	statics: {
		willTransitionFrom: function(transition, component) {
			/*if (component.state.dirty && !confirm('Leave without saving?')) {
				transition.abort();
			}*/
		}
	},
	
    getInitialState:function(){
    	return {
    		employee :{id:'',firstName:'',lastName:''},
    		errors:{},
    		dirty:false
    	};
    },

    componentWillMount: function() {
		var employeeId = this.props.params.id; 

		if (employeeId) {
			this.setState({employee: EmployeeStore.getEmployeeById(employeeId) });
		}
	},


    setEmployeeState:function(event){
    	this.setState({dirty:true});
    	var field=event.target.name;
    	var value=event.target.value;
    	this.state.employee[field]=value;
    	return this.setState({employee:this.state.employee});
    },
    employeeFormIsValid:function(){
    		var formIsValid =true;
    		this.state.errors={};

   			if(this.state.employee.firstName.length <3){
    			this.state.errors.firstName="first name is invalid";
    			formIsValid=false;
    		}

    		if(this.state.employee.lastName.length <3){
    			this.state.errors.lastName="last name is invalid";
    			formIsValid=false;
    		}

    		this.setState({errors:this.state.errors});
        	return formIsValid;
    },
    saveEmployee:function(event){
    	event.preventDefault();
        	if (!this.employeeFormIsValid()) {
			return;
		}

        
        if (this.state.employee.id) {
             EmployeeActions.updateEmployee(this.state.employee);
		} else {
             EmployeeActions.createEmployee(this.state.employee);
		}
		
		this.setState({dirty: false});
		toastr.success('Employee saved.');
		this.transitionTo('employees');
        
    	//EmployeeApi.saveEmployee(this.state.employee);
   
    },

        render:function(){
        return (
        <div>
          <h1>Manage Employees</h1>
            <EmployeeForm 
            employee={this.state.employee} 
            onChange={this.setEmployeeState} 
            onSave={this.saveEmployee} 
            errors={this.state.errors}/>
        </div>
        );
    }
});

module.exports=ManageEmployees;
