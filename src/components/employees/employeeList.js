"use strict";

var React=require('react');
var Router=require('react-router');
var Link =Router.Link;
var AuthorActions = require('../../actions/employeeActions');
var toastr = require('toastr');

var EmployeeList = React.createClass({
    propTypes:{
   //employees:React.propTypes.array.isRequired
    },
    	deleteEmployee: function(id, event) {
		event.preventDefault();
		EmployeeActions.deleteEmployee(id);
		toastr.success('Employee Deleted');
	},
        
        
        render:function(){
            var createEmployeeRow=function(employee){
              return (
              <tr key={employee.id}>
                  	<td><a href="#" onClick={this.deleteEmployee.bind(this, employee.id)}>Delete</a></td>
                  <td><Link to="editEmployee" params={{id:employee.id}}>{employee.id}</Link></td>
                  <td>{employee.firstName} {employee.lastName}</td>
                  </tr>
              )  
            };
            
        return (
            <div>
             <div className="page-header">
            <h1>Employees</h1>      
            </div>
            <table className="table">
            <thead>
            <th>ID</th>
            <th>Name</th>
            </thead>
            <tbody>
            {this.props.employees.map(createEmployeeRow,this)}
            </tbody>
            </table>
            </div>
        );
    }
});

module.exports=EmployeeList;
