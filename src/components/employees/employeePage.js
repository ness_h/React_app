"use strict";

var React=require('react');
var EmployeeApi =require('../../api/employeeApi');
var EmployeeList =require('./employeeList');
var EmployeeActions= require('../../actions/employeeActions');
var EmployeeStore= require('../../stores/employeeStore');
var Link=require('react-router').Link;


var Employees = React.createClass({
        
        getInitialState:function(){
          return {
              employees: EmployeeStore.getAllEmployees()
          };
        },
        
    	componentWillMount: function() {
            EmployeeStore.addChangeListener(this._onChange);
	},

	
	componentWillUnmount: function() {
        EmployeeStore.removeChangeListener(this._onChange);
	},

	_onChange: function() {
		this.setState({ employees: EmployeeStore.getAllEmployees() });
	},
    
       /* componentDidMount: function() {
        if (this.isMounted()) {
            this.setState({ employees: EmployeeStore.getAllEmployees() });
        }
        },*/
        /*componentWillMount:function(){
            this.setState({employees:EmployeeApi.getAllEmployees()});
        },*/
            
        render:function(){      
        return (
        <div>
            <Link to="addEmployee" className="btn btn-default">Add Employee</Link>
            <EmployeeList employees={this.state.employees}/>
        </div>
        );
    }
});

module.exports=Employees;

