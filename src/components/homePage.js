"use strict";

var React = require('react');
var Router =require('react-router');
var Link=Router.Link;

var Home = React.createClass ({
render: function () {
    return ( 
  <div className="container">
  <div className="page-header">
  <h1>Page Header</h1>      
  </div>
  <Link to="about" className="btn btn-primary btn-lg">Check About</Link>
  <p>This is a test.</p>
        
  </div>
    )
}
});

module.exports=Home;