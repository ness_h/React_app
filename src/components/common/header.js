"use strict";

var React=require('react');
var Router =require('react-router');
var Link=Router.Link;


const divStyle = {
  height: '20px'
}; 


var Header = React.createClass({
    render:function(){
        return (
        <nav className="navbar navbar-default">
        <div className="container-fluid">
          <div className="navbar-header">
            <Link to="app" className="navbar-brand">
            <img style={divStyle} src="images/logo.jpg"/> </Link>
            <a className="navbar-brand" >Test</a>
          </div>
          <div id="navbar" className="navbar-collapse collapse">
            <ul className="nav navbar-nav">
              <li><Link to="app">Home</Link></li>
              <li><Link to="about">About</Link></li>
            <li><Link to="employees">Employees</Link></li>
            </ul>
          </div>
        </div>
      </nav>
        )
    }
});

module.exports=Header;

