"use strict";

var keyMirror = require("react/lib/keyMirror");

module.exports= keyMirror({
  INITIALIZE: null,
	CREATE_EMPLOYEE: null,
	UPDATE_EMPLOYEE: null,
	DELETE_EMPLOYEE: null
    
});

